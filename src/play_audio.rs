// audio.rs
//
// Copyright 2018 Peter Sonntag <peter@sonniger-tag.eu>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

extern crate gstreamer as gst;
extern crate gstreamer_player as gst_player;

use self::gst::prelude::*;
use std::thread;

use gst::glib;

use std::sync::{Arc, Mutex};

pub fn play_push_up(i: usize) {
    let names = ["whoo", "1", "2", "3", "boing"];
    let i = i.min(names.len() - 1);
    play_audio(String::from(names[i]));
}

pub fn play_boing() {
    play_audio(String::from("boing"));
}

pub fn play_get_ready() {
    play_audio(String::from("tut_tut"));
}

/// Plays a audio with name.ogg
fn play_audio(name: String) {
    thread::spawn(move || {
        gst::init().unwrap();
        let main_loop = glib::MainLoop::new(None, false);

        let dispatcher = gst_player::PlayerGMainContextSignalDispatcher::new(None);
        let player = gst_player::Player::new(
            None,
            Some(&dispatcher.upcast::<gst_player::PlayerSignalDispatcher>()),
        );
        // Tell the player what uri to play.
        player.set_uri(Some(&format!("resource:///org/pushupmaster/{name}.ogg")));

        let error = Arc::new(Mutex::new(Ok(())));

        let main_loop_clone = main_loop.clone();
        // Connect to the player's "end-of-stream" signal, which will tell us when the
        // currently played media stream reached its end.
        player.connect_end_of_stream(move |player| {
            let main_loop = &main_loop_clone;
            player.stop();
            main_loop.quit();
        });

        let main_loop_clone = main_loop.clone();
        let error_clone = Arc::clone(&error);
        // Connect to the player's "error" signal, which will inform us about eventual
        // errors (such as failing to retrieve a http stream).
        player.connect_error(move |player, err| {
            let main_loop = &main_loop_clone;
            let error = &error_clone;

            *error.lock().unwrap() = Err(err.clone());

            player.stop();
            main_loop.quit();
        });

        player.play();
        main_loop.run();
    });
}
