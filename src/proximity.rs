use crate::application::AppModel;
use crate::application::AppMsg::{self, *};
use core::time;
use relm4::{send, MessageHandler, Sender};
use std::{
    fs::File,
    io::Read,
    sync::mpsc::{channel, TryRecvError},
    thread,
};

const PATH: &str = "/sys/bus/iio/devices/iio:device1/in_proximity_raw";
const UPPER_LIMIT: u32 = 170;
const LOWER_LIMIT: u32 = 150;

pub struct ProximityHandler {
    sender: std::sync::mpsc::Sender<ProximityMsg>,
}

#[derive(Debug, PartialEq)]
pub enum ProximityMsg {
    StartNormal,
    StartAllYouCanDo,
    Stop,
}

impl MessageHandler<AppModel> for ProximityHandler {
    type Msg = ProximityMsg;
    type Sender = std::sync::mpsc::Sender<ProximityMsg>;

    fn init(_parent_model: &AppModel, parent_sender: Sender<AppMsg>) -> Self {
        let (sender, receiver) = channel();

        thread::spawn(move || {
            loop {
                // Wait for start message
                let message = receiver.recv().unwrap();
                let mut above = false;
                // loop as long channel is empty
                while receiver.try_recv().err() == Some(TryRecvError::Empty) {
                    thread::sleep(time::Duration::from_millis(1));
                    if let Ok(mut sensor) = File::open(PATH) {
                        let mut value = String::new();
                        let _ = sensor.read_to_string(&mut value);
                        let value = value.trim();
                        if value.parse::<u32>().unwrap_or_default() > UPPER_LIMIT && !above {
                            above = true;
                            match message {
                                ProximityMsg::StartAllYouCanDo => {
                                    send!(parent_sender, CountUpPushupCounter)
                                }
                                ProximityMsg::StartNormal => {
                                    send!(parent_sender, CountDownPushupCounter)
                                }
                                ProximityMsg::Stop => {}
                            }
                        }

                        if value.parse::<u32>().unwrap_or_default() < LOWER_LIMIT && above {
                            above = false;
                        }
                    }
                }
            }
        });

        ProximityHandler { sender }
    }

    fn send(&self, msg: Self::Msg) {
        let _ = self.sender.send(msg).is_err();
    }

    fn sender(&self) -> Self::Sender {
        self.sender.clone()
    }
}
