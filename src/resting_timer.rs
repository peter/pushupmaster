use core::time;
use std::{
    sync::mpsc::{channel, TryRecvError},
    thread,
};
use relm4::{send, MessageHandler, Sender};
use crate::application::{AppModel, AppMsg};

pub struct RestingTimerHandler {
    sender: std::sync::mpsc::Sender<RestingTimerMsg>,
}

#[derive(Debug)]
pub enum RestingTimerMsg {
    StartStopTimer,
}

impl MessageHandler<AppModel> for RestingTimerHandler {
    type Msg = RestingTimerMsg;
    type Sender = std::sync::mpsc::Sender<RestingTimerMsg>;

    fn init(_parent_model: &AppModel, parent_sender: Sender<AppMsg>) -> Self {
        let (sender, receiver) = channel();
        thread::spawn(move || {
            loop {
                // Wait for start message
                let _ = receiver.recv();
                // loop as long channel is empty
                while receiver.try_recv().err() == Some(TryRecvError::Empty) {
                    thread::sleep(time::Duration::from_secs(1));
                    send!(parent_sender, AppMsg::CountDownRestingTimer);
                }
            }
        });

        RestingTimerHandler { sender }
    }

    fn send(&self, msg: Self::Msg) {
        self.sender.send(msg).unwrap();
    }

    fn sender(&self) -> Self::Sender {
        self.sender.clone()
    }
}
