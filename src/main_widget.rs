use gettextrs::gettext;
use gtk::prelude::{BoxExt, ButtonExt, GridExt, GtkWindowExt, OrientableExt, WidgetExt};
use gtk::Align;
use relm4::{
    adw::{self, traits::AdwApplicationWindowExt},
    send, Widgets,
};
use crate::application::*;

#[relm4::widget(pub)]
impl Widgets<crate::application::AppModel, ()> for AppWidgets {
    view! {
        adw::ApplicationWindow {
            set_title: Some(&gettext("Pushup Master")),
            set_default_width: 300,
            set_default_height: 100,
            set_content = Some(&gtk::Box) {
                set_orientation: gtk::Orientation::Vertical,
                append = &adw::HeaderBar {
                    pack_start: start_stop_button = &gtk::Button {
                        set_label: "Start",
                        set_visible: watch!(model.show_button == ShowButton::StartStop),
                        connect_clicked(sender) => move |_| {
                            send!(sender, AppMsg::StartTraining);
                        }
                    },
                    pack_start = &gtk::Button::with_label(&gettext("Give up!")) {
                        set_visible: watch!(model.show_button == ShowButton::GiveUp),
                        connect_clicked(sender) => move |_| {
                        send!(sender, AppMsg::GiveUp);
                        },
                    },
                    pack_start = &gtk::Button::with_label(&gettext("I'm finally done!")) {
                        set_visible: watch!(model.show_button == ShowButton::Done),
                        connect_clicked(sender) => move |_| {
                        send!(sender, AppMsg::IAmDone);
                        },
                    },
                    pack_start = &gtk::Button::with_label(&gettext("Skip")) {
                        set_visible: watch!(model.show_button == ShowButton::Skip),
                        connect_clicked(sender) => move |_| {
                        send!(sender, AppMsg::SkipResting);
                    }},
                },
                append: stack = &gtk::Stack {
                    set_transition_type: gtk::StackTransitionType::SlideLeftRight,
                    add_child: overview = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        append = &gtk::StackSwitcher {
                        },
                        append = &gtk::Stack {
                            add_child = &gtk::Box {
                                set_orientation: gtk::Orientation::Vertical,
                                append = &gtk::Label {
                                    set_markup: &gettext("<b>Summary:</b>"),
                                    set_margin_top: 10,
                                    set_margin_bottom: 5,
                                },
                                append =  &gtk::Box {
                                    set_halign: Align::Center,
                                    append =  &gtk::Label {
                                        set_label: &gettext("Your Status: "),
                                    },
                                    append =  &gtk::Label {
                                        set_label: watch!(&model.training.get_title()),
                                    },
                                },
                                append = &gtk::Box {
                                    set_halign: Align::Center,
                                    append = &gtk::Label {
                                        set_label: &gettext("Start:"),
                                    },
                                    append = &gtk::Label {
                                        set_label: watch!(&model.training.get_can_do().to_string()),
                                        set_margin_end: 30,
                                    },
                                    append = &gtk::Label {
                                        set_label: &gettext("Goal:"),
                                    },
                                    append = &gtk::Label {
                                        set_label: watch!(&model.training.get_goal().to_string()),
                                    },
                                },
                                append = &gtk::Label {
                                    set_markup: &gettext("<b>Your next Intervals:</b>"),
                                    set_margin_top: 20,
                                    set_margin_bottom: 10,
                                },
                                append = &gtk::Grid {
                                    set_row_spacing: 10,
                                    set_column_spacing: 5,
                                    set_halign: Align::Center,
                                    set_margin_bottom: 20,
                                    attach(1, 1, 1, 1) = &gtk::Label {
                                        set_label: &gettext("Day 1"),
                                        set_halign: Align::Center,
                                    },
                                    attach(2, 1, 1, 1) = &gtk::Label {
                                        set_markup: watch!(&*model.training.trainings_plan_as_markup(0)),},
                                    attach(3, 1, 1, 1) = &gtk::Image {
                                        set_from_icon_name: watch!(Some(model.training.get_progress_icon_name(1)))
                                    },
                                    attach(2, 2, 1, 1) = &gtk::Label {
                                        set_label: &text_one_day_break,
                                    },
                                    attach(1, 3, 1, 1) = &gtk::Label {
                                        set_label: &gettext("Day 3"),
                                        set_halign: Align::Center,
                                    },
                                    attach(2, 3, 1, 1) = &gtk::Label {
                                        set_markup: watch!(&*model.training.trainings_plan_as_markup(1))},
                                    attach(3, 3, 1, 1) = &gtk::Image {
                                        set_from_icon_name: watch!(Some(model.training.get_progress_icon_name(2)))
                                    },
                                    attach(2, 4, 1, 1) = &gtk::Label {
                                        set_label: &text_one_day_break,
                                    },
                                    attach(1, 5, 1, 1) = &gtk::Label {
                                        set_label: &gettext("Day 5"),
                                        set_halign: Align::Center,
                                    },
                                    attach(2, 5, 1, 1) = &gtk::Label {
                                        set_markup: watch!(&*model.training.trainings_plan_as_markup(2)),                                    },
                                    attach(3, 5, 1, 1) = &gtk::Image {
                                        set_from_icon_name: watch!(Some(model.training.get_progress_icon_name(3)))
                                    },
                                    attach(2, 6, 1, 1) = &gtk::Label {
                                        set_label: &text_one_day_break,
                                    },
                                    attach(1, 7, 1, 1) = &gtk::Label {
                                        set_label: &gettext("Day 7"),
                                        set_halign: Align::Center,
                                    },
                                    attach(2, 7, 1, 1) = &gtk::Label {
                                        set_label: &gettext("As many as you can"),
                                    },
                                    attach(3, 7, 1, 1) = &gtk::Image {
                                        set_from_icon_name: watch!(Some(model.training.get_progress_icon_name(4)))
                                    },
                                }
                            }
                        },
                    },
                    add_child: initialization = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        append=&gtk::Label {
                            set_margin_top: 30,
                            set_margin_bottom: 20,
                            set_markup: &gettext("<b>First time setup</b>"),
                        },
                        append=&gtk::Label {
                            set_label: &gettext("How many push-ups can you do?"),
                            set_margin_bottom: 5,
                        },
                        append: can_do_spin = &gtk::SpinButton {
                            set_margin_start: 100,
                            set_margin_end: 100,
                            set_margin_bottom: 20,
                            set_range: args!(5.,20.),
                            set_increments: args!(1.,10.),
                            connect_value_changed(sender) => move |can_do_spin| {
                                send!(sender, AppMsg::SetCanDoPushups(can_do_spin.value_as_int() as usize))
                            }
                        },
                        append=&gtk::Label {
                            set_label: &gettext("What is your goal?"),
                            set_margin_bottom: 5,
                        },
                        append: target_spin =&gtk::SpinButton {
                            set_margin_start: 100,
                            set_margin_end: 100,
                            set_margin_bottom: 50,
                            set_range: args!(20.,200.),
                            set_increments: args!(1.,10.),
                            connect_value_changed(sender) => move |target_spin| {
                                send!(sender, AppMsg::SetTargetPushups(target_spin.value_as_int() as usize))
                            }
                        },
                        append =&gtk::Button {
                            set_margin_start: 100,
                            set_margin_end: 100,
                            set_label: &gettext("Start Training"),
                            connect_clicked(sender) => move |_| {
                                send!(sender, AppMsg::InitTraining);
                            }
                        },
                    },
                    add_child: push_ups = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        append = &gtk::Button {
                            set_label : watch!(&model.remaining_pushups.to_string()),
                            set_vexpand: true,
                            set_margin_top: 20,
                            set_margin_bottom: 20,
                            set_margin_start: 20,
                            set_margin_end: 20,
                            connect_clicked(sender) => move |_| {
                                send!(sender, AppMsg::CountDownPushupCounter);
                            }
                        },
                    },
                    add_child: as_many_push_ups_as_possible = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        append = &gtk::Button {
                            set_label: watch!(&model.as_many_as_possible_pushups.to_string()),
                            set_vexpand: true,
                            set_margin_top: 20,
                            set_margin_bottom: 20,
                            set_margin_start: 20,
                            set_margin_end: 20,
                            connect_clicked(sender) => move |_| {
                                send!(sender, AppMsg::CountUpPushupCounter);
                            }
                        },
                    },
                    add_child: resting = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        append = &gtk::Label {
                            set_markup: watch!(&format!("<span size=\"x-large\">{}</span>",&model.remaining_seconds_to_wait.unwrap_or(0))),
                            set_vexpand: true,
                        },
                    }
                },
            },
        }
    }

    fn pre_init() {
        let text_one_day_break = gettext("one day break");
    }

    fn pre_view() {
        match model.page {
            Page::Overview => {
                self.stack.set_visible_child(&self.overview);
                self.start_stop_button.show();
            }
            Page::Initialization => {
                self.stack.set_visible_child(&self.initialization);
                self.start_stop_button.hide();
            }
            Page::Pushups => self.stack.set_visible_child(&self.push_ups),
            Page::Resting => self.stack.set_visible_child(&self.resting),
            Page::AsManyPushupsAsPossible => self
                .stack
                .set_visible_child(&self.as_many_push_ups_as_possible),
        }
    }

    fn post_init() {
        match model.page {
            Page::Overview => stack.set_visible_child(&overview),
            Page::Initialization => stack.set_visible_child(&initialization),
            _ => panic!("Not expected page"),
        }
        can_do_spin.set_value(model.init_pushups_can_do as f64);
        target_spin.set_value(model.init_pushups_target as f64);
    }
}
