use crate::main_widget::AppWidgets;
use crate::proximity::{ProximityHandler, ProximityMsg};
use crate::resting_timer::{RestingTimerHandler, RestingTimerMsg};
use crate::{
    play_audio,
    training::{self, *},
};

use relm4::RelmMsgHandler;
use relm4::{send, AppUpdate, Model, Sender};
use std::time::{Duration, Instant};

#[derive(Clone, Copy, Debug)]
pub enum Page {
    Overview,
    Initialization,
    Pushups,
    Resting,
    AsManyPushupsAsPossible,
}

pub struct AppModel {
    pub page: Page,
    pub training: training::Training,
    pub remaining_pushups: usize,
    pub remaining_seconds_to_wait: Option<usize>,
    pub as_many_as_possible_pushups: usize,
    pub init_pushups_target: usize,
    pub init_pushups_can_do: usize,
    pub show_button: ShowButton,
    time_last_push_up: Instant,
}

impl AppModel {
    pub fn new(page: Page, training: Training, show_button: ShowButton) -> AppModel {
        AppModel {
            page,
            training,
            init_pushups_target: 30,
            init_pushups_can_do: 10,
            remaining_pushups: 0,
            remaining_seconds_to_wait: None,
            as_many_as_possible_pushups: 0,
            show_button,
            time_last_push_up: Instant::now(),
        }
    }
}

#[derive(PartialEq, Eq)]
pub enum ShowButton {
    None,
    StartStop,
    Done,
    GiveUp,
    Skip,
}

pub enum AppMsg {
    InitTraining,
    StartTraining,
    TrainingsSessionDone,
    SetTargetPushups(usize),
    SetCanDoPushups(usize),
    CountDownPushupCounter,
    CountUpPushupCounter,
    CountDownRestingTimer,
    StartResting,
    SkipResting,
    IAmDone,
    GiveUp,
    SetOverview,
}

#[derive(relm4::Components)]
pub struct AppComponents {
    proximity: RelmMsgHandler<ProximityHandler, AppModel>,
    resting_timer: RelmMsgHandler<RestingTimerHandler, AppModel>,
}

impl Model for AppModel {
    type Msg = AppMsg;
    type Widgets = AppWidgets;
    type Components = AppComponents;
}

impl AppUpdate for AppModel {
    fn update(&mut self, msg: AppMsg, components: &AppComponents, sender: Sender<AppMsg>) -> bool {
        match msg {
            AppMsg::InitTraining => {
                self.training =
                    Training::create(self.init_pushups_can_do, self.init_pushups_target);
                send!(sender, AppMsg::SetOverview);
            }
            AppMsg::SetTargetPushups(target) => {
                self.init_pushups_target = target;
            }
            AppMsg::SetCanDoPushups(can_do) => {
                self.init_pushups_can_do = can_do;
            }
            AppMsg::StartTraining => {
                components.proximity.send(self.training.get_session_type());
                if self.training.get_day() < 3 {
                    self.show_button = ShowButton::GiveUp;
                    self.remaining_pushups = self.training.get_current();
                    self.page = Page::Pushups;
                } else {
                    self.show_button = ShowButton::Done;
                    self.as_many_as_possible_pushups = 0;
                    self.training.reset_index_day();
                    self.page = Page::AsManyPushupsAsPossible;
                }
            }
            AppMsg::CountDownPushupCounter => {
                // TODO: In order to avoid double counting when proximity and button is triggered
                // we wait 1s --> maybe there is a better solution
                if self.time_last_push_up.elapsed() >= Duration::from_secs(1) {
                    self.time_last_push_up = Instant::now();
                    self.remaining_pushups -= 1;
                    play_audio::play_push_up(self.remaining_pushups);
                    if self.remaining_pushups == 0 {
                        send!(sender, AppMsg::TrainingsSessionDone);
                    }
                }
            }
            AppMsg::CountDownRestingTimer => {
                if let Some(remaining_seconds) = &mut self.remaining_seconds_to_wait {
                    if *remaining_seconds > 0 {
                        if *remaining_seconds == 1 {
                            play_audio::play_get_ready();
                        }
                        *remaining_seconds -= 1;
                    } else {
                        self.remaining_seconds_to_wait = None;
                        components
                            .resting_timer
                            .send(RestingTimerMsg::StartStopTimer);
                        send!(sender, AppMsg::StartTraining);
                    }
                }
            }
            AppMsg::StartResting => {
                self.remaining_seconds_to_wait = Some(self.training.get_current_rest_time());
                self.show_button = ShowButton::Skip;
                self.page = Page::Resting;
                components
                    .resting_timer
                    .send(RestingTimerMsg::StartStopTimer);
            }
            AppMsg::SkipResting => {
                components
                    .resting_timer
                    .send(RestingTimerMsg::StartStopTimer);
                send!(sender, AppMsg::StartTraining);
            }
            AppMsg::GiveUp => {
                self.training.make_training_easier();
                send!(sender, AppMsg::TrainingsSessionDone);
            }
            AppMsg::CountUpPushupCounter => {
                // TODO: In order to avoid double counting when proximity and button is triggered
                // we wait 1s --> maybe there is a better solution
                if self.time_last_push_up.elapsed() >= Duration::from_secs(1) {
                    self.time_last_push_up = Instant::now();
                    self.as_many_as_possible_pushups += 1;
                    play_audio::play_boing();
                }
            }
            AppMsg::IAmDone => {
                components.proximity.send(ProximityMsg::Stop);
                //  Recreate new training with new start value
                self.training = training::Training::create(
                    self.as_many_as_possible_pushups,
                    self.training.get_goal(),
                );
                send!(sender, AppMsg::SetOverview);
            }
            AppMsg::SetOverview => {
                self.show_button = ShowButton::StartStop;
                self.training.save();
                self.page = Page::Overview;
            }
            AppMsg::TrainingsSessionDone => {
                // All pushups done
                components.proximity.send(ProximityMsg::Stop);
                if self.training.next_index() {
                    send!(sender, AppMsg::StartResting)
                } else {
                    // Last session, back to overview
                    self.training.next_day();
                    send!(sender, AppMsg::SetOverview);
                }
            }
        }
        true
    }
}
