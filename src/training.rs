// training.rs
//
// Copyright 2022 Peter Sonntag <peter@sonniger-tag.eu>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

/// Factor for calculating trainings session
const FACTOR: [[f32; 5]; 3] = [
    [0.7, 0.66, 0.5, 0.5, 0.4],
    [0.77, 0.67, 0.55, 0.55, 0.46],
    [0.83, 0.73, 0.63, 0.63, 0.53],
];

use gettextrs::gettext;
use serde::{Deserialize, Serialize};
use std::{
    cmp, error,
    fs::File,
    io::{Read, Write},
};

use crate::proximity::ProximityMsg;

const FILE_NAME: &str = "training.cfg";
const APP_PREFIX: &str = "pushupmaster";

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Training {
    /// Number of pushups per set
    pushups: Vec<Vec<usize>>,
    /// done days
    day: usize,
    index: usize,
    active: bool,
    init: bool,
    can_do: usize,
    goal: usize,
}

impl Training {
    /// provides an empty trainings plan
    pub fn new() -> Training {
        Training {
            pushups: vec![vec![]; 3],
            day: 0,
            index: 0,
            active: false,
            can_do: 0,
            goal: 0,
            init: false,
        }
    }

    /// Reads the trainings settings from the init file
    pub fn load() -> Option<Training> {
        if let Ok(training) = Training::parse_from_file() {
            Some(training)
        } else {
            None
        }
    }

    fn parse_from_file() -> Result<Self, Box<dyn error::Error>> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix(APP_PREFIX).unwrap();
        let path = xdg_dirs.get_config_file(FILE_NAME);
        let mut file = File::open(path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        Ok(serde_json::from_str(&contents)?)
    }

    pub fn save(&self) {
        let xdg_dirs = xdg::BaseDirectories::with_prefix(APP_PREFIX).unwrap();
        let path = xdg_dirs.place_config_file(FILE_NAME).unwrap();
        let mut file = File::create(path).unwrap();
        file.write_all(serde_json::to_string_pretty(&self).unwrap().as_bytes())
            .unwrap();
    }

    /// Creates an new trainings plan <br>
    /// * `can_do` - user is able to perform currently that many pushups<br>
    /// * `goal` - user wants to be able to perform that many pushups
    pub fn create(can_do: usize, goal: usize) -> Training {
        // multiply factor entries with can_do to calculate push ups
        let pushups: Vec<Vec<usize>> = FACTOR
            .iter()
            .map(|a| a.iter().map(|f| (f * can_do as f32) as usize).collect())
            .collect();

        Training {
            pushups,
            day: 0,
            index: 0,
            can_do,
            goal,
            active: false,
            init: true,
        }
    }

    /// The user's achievement title derived
    /// from the number of pushups he/she can do
    pub fn get_title(&self) -> String {
        // 0..=10 - 190..200
        let title: [String; 20] = [
            gettext("Newcomer"),      //  -10
            gettext("Rookie"),        //  -20
            gettext("Good in Shape"), //  -30
            gettext("Advanced"),      //  -40
            gettext("Sporty"),        //  -50
            gettext("Super Fit"),     //  -60
            gettext("Professional"),  //  -70
            gettext("Hero"),          //  -80
            gettext("Popeye"),        //  -90
            gettext("Star"),          // -100
            gettext("King"),          // -110
            gettext("Taurus"),        // -120
            gettext("Hammer"),        // -130
            gettext("Superstar"),     // -140
            gettext("Muscleman"),     // -150
            gettext("Megastar"),      // -160
            gettext("Machine"),       // -170
            gettext("Superman"),      // -180
            gettext("Hulk"),          // -190
            gettext("God"),           // -200
        ];
        let mut i = (cmp::max(self.can_do, 1) - 1) / 10;
        i = cmp::min(i, title.len());

        title[i].clone()
    }

    /// Will recalculate the trainings plan to make it easier
    pub fn make_training_easier(&mut self) {
        let new_plan = Training::create(1.max(self.can_do - 1), self.goal).pushups;
        // update future plan in current session
        if self.index < 3 {
            for index in (self.index + 1)..=4 {
                self.pushups[self.day][index] = new_plan[self.day][index];
            }
        }
        // update plans of future days
        if self.day < 2 {
            for day in (self.day + 1)..3 {
                self.pushups[day] = new_plan[day].clone();
            }
        }
    }

    /// returns a displayable trainings plan for a days
    pub fn trainings_plan_as_markup(&self, d: usize) -> String {
        if d < 3 {
            let mut s = String::new();
            for i in 0..self.pushups[d as usize].len() {
                if i == self.index as usize && self.active {
                    s.push_str(&format!(
                        "<span foreground=\"blue\"><b>{}</b></span>-",
                        self.pushups[d as usize][i]
                    ))
                } else {
                    s.push_str(&format!("{}-", self.pushups[d as usize][i]))
                }
            }
            s.pop();
            s
        } else {
            String::from("")
        }
    }

    pub fn get_progress_icon_name(&self, d: usize) -> &str {
        if self.day < d {
            "checkbox-symbolic"
        } else {
            "checkbox-checked-symbolic"
        }
    }

    pub fn get_day(&self) -> usize {
        self.day
    }

    pub fn reset_index_day(&mut self) {
        self.day = 0;
        self.index = 0;
    }

    pub fn next_day(&mut self) {
        self.index = 0;
        self.day += 1;
    }

    pub fn get_session_type(&self) -> ProximityMsg {
        if self.day > 2 {
            ProximityMsg::StartAllYouCanDo
        } else {
            ProximityMsg::StartNormal
        }
    }

    pub fn get_can_do(&self) -> usize {
        self.can_do
    }

    pub fn get_goal(&self) -> usize {
        self.goal
    }

    /// returns the number of pushups for the next session
    pub fn get_current(&self) -> usize {
        self.pushups[self.day as usize][self.index as usize]
    }

    pub fn get_current_rest_time(&self) -> usize {
        let n_of_last_pushups = self.pushups[self.day as usize][self.index as usize - 1] as f32;

        120 + (((n_of_last_pushups - 15.) * 0.7) as usize).min(80)
    }

    pub fn next_index(&mut self) -> bool {
        if self.index < self.pushups[self.day].len() - 1 {
            self.index += 1;
            true
        } else {
            false
        }
    }
}
