mod application;
mod config;
mod play_audio;
mod proximity;
mod resting_timer;
mod training;
mod main_widget;

extern crate xdg;

use application::{Page, ShowButton};
use config::{GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::gio;
use relm4::RelmApp;
use training::Training;

fn main() {
    // Set up gettext translations
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/pushupmaster.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    let (training, page, show_button) = if let Some(plan) = Training::load() {
        (plan, Page::Overview, ShowButton::StartStop)
    } else {
        (Training::new(), Page::Initialization, ShowButton::None)
    };

    let model = application::AppModel::new(page, training, show_button);

    let app = RelmApp::new(model);
    app.run();
}
